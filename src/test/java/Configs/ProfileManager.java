package Configs;

import java.io.*;
import java.util.Properties;

public class ProfileManager {
   static protected ProfileManager instance;
   static protected Properties prop = new Properties();
   static protected String pathOfConfigurationFile;
   static protected String environment;
   static protected String browser;

   /***
    * Thanks to this method we create an unique instance to load all the data at the first call of the others method.
    * This avoid to load the data at static method called.
    * */
   static public void getInstance(){
       if(instance == null){
           instance = new ProfileManager();
           instance.dataLoader();
       }
   }

    /***
     * Thank to this method, we reload the file's data after being modified.
     */
    static public void resetInstance(){
       instance = null;
       getInstance();
    }

    /***
     * Get the absolute path of the right configuration file.
     * @return String
     */
    static public String getUri(){
       File file = new File("src\\test\\resources\\profile.properties");
       return file.getAbsolutePath();
   }

    /***
     * Load all the data of the file and put it in the class variables
     */
   static public void dataLoader(){
        try {
            pathOfConfigurationFile = getUri();
            InputStream fis = new FileInputStream(pathOfConfigurationFile);
            prop.load(fis);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + pathOfConfigurationFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
       environment = prop.getProperty("environment");
       browser = prop.getProperty("browser");
    }

    /***
     * Write and save a data on the configuration file.
     * @param key
     * @param value
     */
    static public void configurationWriter(String key, String value) {
       getInstance();
       try {
            pathOfConfigurationFile = getUri();
            OutputStream fis = new FileOutputStream(pathOfConfigurationFile);
            prop.setProperty(key, value);
            prop.store(fis, null);
            fis.close();
            resetInstance();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + pathOfConfigurationFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /***
     * Get the environment name from the configuration file.
     * @return
     */
    static public String getEnvironmentName(){
       getInstance();
       if(environment != null){
           return environment;
       }else throw new RuntimeException("Environment not specified in the Configuration.properties file.");
    }

    /***
     * Get the browser name from the configuration file.
     * @return
     */
    static public String getBrowserName(){
       getInstance();
        if(browser != null){
            return browser;
        }else throw new RuntimeException("browser not specified in the Configuration.properties file.");
    }

}
