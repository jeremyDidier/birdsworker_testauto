package Configs;

import java.io.*;
import java.util.Properties;

public class EnvironmentVariables {
    protected static EnvironmentVariables instance;
    protected static String pathOfEnvironmentFile;
    protected static String environmentName;
    protected static Properties prop = new Properties();


    /**
     * Thanks to this method we create an unique instance to load all the data at the first call of the others method.
     * This avoid to load the data at static method called.
     */
    public static void getInstance(){
        if(instance == null){
            instance = new EnvironmentVariables();
            instance.dataLoader();
        }
    }

    /**
     * Thank to this method, we reload the file's data after being modified.
     */
    public static void resetInstance(){
        instance = null;
        getInstance();
    }

    /**
     * Thank to this method we make sur that we are still in the same environment.
     */
    public static void getEnvironment(){
        if (environmentName != ProfileManager.getEnvironmentName().toLowerCase()){
            instance.dataLoader();
        }
    }

    /**
     * Get the absolute path of the right environment file.
     * To get that we call the environment name from the configuration file and put it in the URI.
      * @return String
     */
    public static String getUri(){
       environmentName = ProfileManager.getEnvironmentName().toLowerCase();
       File file = new File("src\\test\\resources\\environments\\"+environmentName+".properties");
       return file.getAbsolutePath();
   }

    /**
     * Load all the data of the file and put it in the class variables
     */
    public static void dataLoader(){
       try {
           pathOfEnvironmentFile = getUri();
           InputStream fis = new FileInputStream(pathOfEnvironmentFile);
           prop.load(fis);
           fis.close();
       } catch (FileNotFoundException e) {
           e.printStackTrace();
           throw new RuntimeException("Configuration.properties not found at " + pathOfEnvironmentFile);
       } catch (IOException e) {
           e.printStackTrace();
       }
    }

    /**
     * Write and save a data on the environment file.
     * @param key
     * @param value
     */
    public static void profileWriter(String key, String value){
        getInstance();
        getEnvironment();//Just before we made sur that the instance is on. And now, we verify that the environment is correct
        try {
            pathOfEnvironmentFile = getUri();
            OutputStream fos = new FileOutputStream(pathOfEnvironmentFile);
            prop.setProperty(key,value);
            prop.store(fos, null);
            fos.close();
            resetInstance();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + pathOfEnvironmentFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /***
     * Return the wanted variable from the file
     * @return
     */
    public static String getVariable(String variable) {
        getInstance();
        getEnvironment();
        String wantedVariable = prop.getProperty(variable);
        if (wantedVariable != null) {
            return wantedVariable;
        } else throw new RuntimeException("passwordConsultant not specified in the "+environmentName+".properties file.");
    }

}
