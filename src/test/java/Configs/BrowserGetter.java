package Configs;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class BrowserGetter {
    static protected WebDriver driver;

    /***
     * Get the browser's name from the configuration file and then according to that name, put the right driver.
     * @return
     */
   static public WebDriver getDriver(){
       String browser = ProfileManager.getBrowserName().toLowerCase();
       if(browser.equalsIgnoreCase("chrome")){
           WebDriverManager.chromedriver().setup();
           driver = new ChromeDriver();
       }else if(browser.equalsIgnoreCase("firefox")){
           WebDriverManager.firefoxdriver().setup();
           driver = new FirefoxDriver();
       }else if(browser.equalsIgnoreCase("edge")){
           WebDriverManager.edgedriver().setup();
           driver = new EdgeDriver();
       }else if(browser.equalsIgnoreCase("chrome_headless")){
           WebDriverManager.chromedriver().setup();
           ChromeOptions options = new ChromeOptions();
           options.setHeadless(true);
           driver = new ChromeDriver(options);
       }else if(browser.equalsIgnoreCase("firefox_headless")){
           WebDriverManager.firefoxdriver().setup();
           FirefoxOptions options = new FirefoxOptions();
           options.setHeadless(true);
           driver = new FirefoxDriver(options);
       }else{
           throw new RuntimeException("The called browser is not supported = "+browser);
       }
       //Be careful, I have to set size to chrome_headless because the maximise method is not working on it.
       if(browser.equalsIgnoreCase("chrome_headless")){
           driver.manage().window().setSize(new Dimension(1600,700));
       }else{
           driver.manage().window().maximize();
       }
        return driver;
    }

    /***
     * This method will setup the driver according to the constructor method
     * @param browser
     * @return
     */
    static public WebDriver getDriver(String browser){
        if(browser.equalsIgnoreCase("chrome")){
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        }else if(browser.equalsIgnoreCase("firefox")){
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        }else if(browser.equalsIgnoreCase("edge")){
            WebDriverManager.edgedriver().setup();
            driver = new EdgeDriver();
        }else if(browser.equalsIgnoreCase("chrome_headless")){
            WebDriverManager.chromedriver().setup();
            ChromeOptions options = new ChromeOptions();
            options.setHeadless(true);
            driver = new ChromeDriver(options);
        }else if(browser.equalsIgnoreCase("firefox_headless")){
            WebDriverManager.firefoxdriver().setup();
            FirefoxOptions options = new FirefoxOptions();
            options.setHeadless(true);
            driver = new FirefoxDriver(options);
        }else{
            throw new RuntimeException("The called browser is not supported = "+browser);
        }
        //Be careful, I have to set size to chrome_headless because the maximise method is not working on it.
        if(browser.equalsIgnoreCase("chrome_headless")){
            driver.manage().window().setSize(new Dimension(1600,700));
        }else{
            driver.manage().window().maximize();
        }
        return driver;
    }
}
