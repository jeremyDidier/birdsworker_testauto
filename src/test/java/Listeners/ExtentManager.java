package Listeners;

import Configs.ProfileManager;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.testng.ITestContext;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExtentManager {
    protected static ExtentReports extent;
    protected static ExtentHtmlReporter htmlReporter;
    protected static String browser = ProfileManager.getBrowserName();
    protected static String environment = ProfileManager.getEnvironmentName();
    protected static String suiteName;
    protected static String hostname;

    static {
        try {// get the IP of the computer on which the tests is run.
            hostname = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    /***
     * On calling this method, you instance a report with all the context. Like the name etc...
     * @param context
     * @return
     */
    public static ExtentReports createInstance(ITestContext context){
        //use context to get the suite Name thanks to the current testng.xml
        suiteName = context.getCurrentXmlTest().getSuite().getName();
        String fileName = getReportName();
        String directory = System.getProperty("user.dir")+"\\reports\\";
        new File(directory).mkdirs();// Create all the necessary directories if not exist
        String reportPath = directory+fileName;
        //Instance an html on the right file with the configuration of the exent-file.xml
        htmlReporter = new ExtentHtmlReporter(reportPath);
        htmlReporter.loadConfig(System.getProperty("user.dir")+ "\\extent-config.xml");
        //Create the report, add all the context and attach the html to it.
        extent = new ExtentReports();
        extent.setSystemInfo("Suite Name", suiteName);
        extent.setSystemInfo("Organization", "Birds worker");
        extent.setSystemInfo("Browser", browser);
        extent.setSystemInfo("Host Name", hostname);
        extent.setSystemInfo("Environment", environment);
        extent.setSystemInfo("User Name", "Testeur");
        extent.attachReporter(htmlReporter);

        return extent;
    }

    /***
     * Thanks to this method you can concat the suite Name + the formatted date + the html extention
     * to return a dynamic name of the report.
     * @return
     */
    public static String getReportName(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ssZ");
        String date = simpleDateFormat.format(new Date());
        String fileName = suiteName + "_" + date+".html";
        return fileName;
    }

}
