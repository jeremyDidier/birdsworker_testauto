package Listeners;

import AbstractClass.BaseTest;
import Configs.ProfileManager;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.*;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TestListeners implements ITestListener{

    //Initialize the extent and ExtentTest. ExtentTest need to be ThreadSafe in order to handle parallel test
    private static ExtentReports extent;
    private static ThreadLocal<ExtentTest> extentTest;
    private String browser;
    private String environment;
    private String testSuiteName;

    /***
     * When a test suite start, if a extent reports is not creates, this method will create it.
     * @param context
     */
    @Override
    public void onStart(ITestContext context) {
        getBrowserName(context);
        getEnvironmentName(context);
        getTestSuiteName(context);
        if(extent == null) {
            extent = ExtentManager.createInstance(context);
            extentTest = new ThreadLocal<ExtentTest>();
        }

    }

    /***
     * When a Test Method starts, this method will create a test section for this method on the report
     * with a dynamic name and put it into a thread to not interfer with other parralel tests
     * @param result
     */
    @Override
    public void onTestStart(ITestResult result) {
        String testTitle = getTestTitle(result);
        ExtentTest test = extent.createTest(testTitle);
        extentTest.set(test);
    }

    /***
     * If test succeed mark it as successful
     * @param result
     */
    @Override
    public void onTestSuccess(ITestResult result) {
        String logText = "<b>Test Method " + result.getMethod().getMethodName() + " Successful</b>";
        Markup m = MarkupHelper.createLabel(logText, ExtentColor.GREEN);
        extentTest.get().log(Status.PASS, m);
    }

    /***
     * If test fails. Make 5 operations:
     * 1/ Return a section with the error message
     * 2/ Return an wrap section with all the StackTrace
     * 3/ Return a screenshot of failure.
     * 4/ Mark it Fail.
     * 5/ Mark if the test was rerun before being logged.
     *
     * @param result
     */
    @Override
    public void onTestFailure(ITestResult result) {
        String methodName = result.getMethod().getMethodName();
        String exceptionMessage = Arrays.toString(result.getThrowable().getStackTrace());
        // 1/
        extentTest.get().log(Status.FAIL, result.getThrowable());
        // 2/
        extentTest.get().fail("<details><summary><b><font color=red>Exception Occured, click to see details: "
                + "</font></b></summary>" + exceptionMessage.replaceAll(",", "<br/>") + "</details> \n");
        // 3/
        WebDriver driver = ((BaseTest)result.getInstance()).getDriver();// get
        String base64Screenshot;
        try {// base64 convert an image into String. The allow to include it everywhere and then convert it.
            base64Screenshot = takeScreenshot(driver, methodName);
            extentTest.get().fail("<b><font color=red>"+"Screenshot of Failure"+"</font></b>" , MediaEntityBuilder.createScreenCaptureFromBase64String(base64Screenshot).build());
        } catch (IOException e) {
            extentTest.get().fail("Test Failed, cannot attach screenshot");
        }
        // 4/
        String logText = "<b>Test Method " + methodName + " Failed</b>";
        Markup m = MarkupHelper.createLabel(logText, ExtentColor.RED);
        extentTest.get().log(Status.FAIL, m);
        // 5/ in the TestRetryListener.class, we set the attribute "rerun" when we retry a failed test.
        if(result.getAttributeNames().contains("rerun")){
            extentTest.get().fail("<b><font color=red>"+"This test case was rerun before being logged as Failed"+"</font></b>");
        }else{
            extentTest.get().fail("<b><font color=red>"+"This test case was NOT rerun "+"</font></b>");
        }

    }

    /***
     * On test skipped:
     * If test will be rerun, remove it
     * If not mark it as skipped
     */
    @Override
    public void onTestSkipped(ITestResult result) {
        /* Before to rerun the test (thanks to TestRetryListener.Class),
         * we create an attribute called "willBeRerun".
         * So if the result contains this attribute it will be rerun and so we can delete it
         */
        if(result.getAttributeNames().contains("willBeRerun")){
            extent.removeTest(extentTest.get());
        }else {
            String logText = "<b>Test Method " + result.getMethod().getMethodName() + " Skipped</b>";
            Markup m = MarkupHelper.createLabel(logText, ExtentColor.YELLOW);
            extentTest.get().log(Status.SKIP, m);
        }
    }

    /***
     * This method is not used
     * @param result
     */
    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
    }

    /***
     * When test suite is finish, flush method will update the html file with the test information
     * @param context
     */
    @Override
    public void onFinish(ITestContext context) {
       if(extent != null) {
           extent.flush();
       }
    }

    /***
     * After taking the screenshot, this method does 2 different operations:
     * 1/ It takes the screenshot and put it in the report folder.
     * 2/ It convert the screenshot into a String base64 and return this String.
     * @param driver
     * @param methodName
     * @return
     * @throws IOException
     */
    public static String takeScreenshot(WebDriver driver, String methodName) throws IOException {
        String fileName = getScreenshotName(methodName);// Return a dynamic name
        String directory = System.getProperty("user.dir") + "/screenshots/";
        new File(directory).mkdirs();
        String path = directory+fileName;
        File screenshot = null;
        try {
            screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            // 1/
            FileUtils.copyFile(screenshot, new File(path));
            System.out.println("**********************************");
            System.out.println("This screenshot is stored at "+ path);
            System.out.println("**********************************");
        }catch(IOException e){
            e.printStackTrace();;
        }
            // 2/
        byte[] fileContent = FileUtils.readFileToByteArray(screenshot);
        String base64Screenshot =  "data:image/png;base64," + Base64.getEncoder().encodeToString(fileContent);
        return base64Screenshot;
    }

    /***
     * Create a dynamic name for the screenshot with the test method name + formatted date + png extention.
     * @param methodName
     * @return
     */
    public static String getScreenshotName(String methodName){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ssZ");
        String date = simpleDateFormat.format(new Date());
        String fileName = methodName + "_" + date+".png";
        return fileName;
    }

    /***
     * Get the test suite name from the current testNG xml file.
     * @param context
     */
    public void getTestSuiteName(ITestContext context){
        this.testSuiteName = context.getCurrentXmlTest().getName();
    }

    /***
     * Return the browser name to put it on the test title.
     * If the browser name  is on the test suite parameter, take it.
     * If not, take it from the profil.properties file.
     * @param context
     */
    public void getBrowserName(ITestContext context){
        String browser;
        if(context.getCurrentXmlTest().getParameter("browser")!=null){
            browser = context.getCurrentXmlTest().getParameter("browser");
        }else{
            browser =  ProfileManager.getBrowserName();
        }
        this.browser = browser;
    }

    /***
     * Return the environment to put it on the test title.
     * If the environment is on the test suite parameter, take it.
     * If not, take it from the profil.properties file.
     * @param context
     */
    public void getEnvironmentName(ITestContext context){
        String env;
        if(context.getCurrentXmlTest().getParameter("env")!=null){
            env = context.getCurrentXmlTest().getParameter("env");
        }else{
            env =  ProfileManager.getEnvironmentName();
        }
        this.environment = env;
    }

    /***
     * Construct the name of the test on the report with 5 items:
     * 1/ The test suite name.
     * 2/ The test class name.
     * 3/ The test case name.
     * 4/ The browser
     * 5/ The environment
     * @param result
     * @return
     */
    public String getTestTitle(ITestResult result){
       return   "<font color=#1565C0><font size=2em>Test suite: </font></font>"
                    +"<font size=2em>"+testSuiteName+"</font><br/>"
                +"<font color=#1565C0><font size=2em>Feature: </font></font>"
                    +"<font size=2em>"+result.getTestClass().getRealClass().getSimpleName()+"</font><br/>"
                +"<font color=#1565C0><font size=2em>Test: </font></font>"
                    +"<font size=2em><font weight=bold>"+result.getMethod().getMethodName()+"</font></font><br/>"
                +"<font color=#1565C0><font size=1.9em>Browser: </font></font>"
                    +"<font size=1.9em>"+this.browser+" & </font>"
                +"<font color=#1565C0><font size=1.9em> Environment: </font></font>"
                    +"<font size=1.9em>"+this.environment+"</font>";
    }

}
