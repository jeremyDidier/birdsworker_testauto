package Listeners;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class TestRetryListener implements IRetryAnalyzer {
    private int counter = 0;
    private static int retryLimit = 1;

    /***
     * If the test fails, this method allow the test to be rerun until that the retry limit is reach.
     * @param result
     * @return
     */
    @Override
    public boolean retry(ITestResult result) {
        /* the status of the result is given as an int:
        * 1 = succeed
        * 2 = failure
        * 3 = skipped
        */
        int status = result.getStatus();
        if(!result.isSuccess()) {
            if (counter < retryLimit) {         // If the retry limit hasn't been reached yet
                counter++;                      // Increment the counter
                result.setStatus(status);       // Set the status
                result.setAttribute("willBeRerun",true);// Add an attribute that will be used in TestListeners.class to delete it from the report
                return true;                    // re-run the test
            } else {                            // If If the retry limit has been reached
                result.setAttribute("rerun",true);// Add an attribute that will be used in TestListeners.class to mark on the report whether the test case has been rerun or not
                result.setStatus(status);       // Set the status
                resetCounter();                 // Reset the counter to be used in other test suite if the tests cases is run
            }
        } else{
            result.setStatus(status);           // If test succeed mark it as succeed
        }
        return false;                           // Do not re-run the tests cases
    }

    /***
     * Reset the counter to be used in other test suite if the tests cases is run
     *
     */
    public void resetCounter(){
        counter = 0;
    }
}
