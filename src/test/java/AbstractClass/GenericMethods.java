package AbstractClass;

import java.io.File;
import java.util.Random;

public class GenericMethods {

    /**
     * Make the sleep call easier.
     * @param seconds
     */
    public void sleep(int seconds){
        try {
            Thread.sleep(seconds*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the absolute path from the relative path
     * @param relativePath
     * @return the absolute path as a String
     */
    public String getPath(String relativePath){
        File file = new File(relativePath);
        return file.getAbsolutePath();
    }

    /**
     *
     * @param length
     * @return a random String of the given length
     */
    public String randomString (int length){
        Random rnd = new Random();
        String SALTCHARS = "abcdefghijklmnopqrstuvwxyz";
        StringBuilder sb = new StringBuilder();
        for(int i =0; i< length; i++ ) {
            int index = rnd.nextInt(SALTCHARS.length());
            sb.append(SALTCHARS.charAt(index));
        }
        return sb.toString();
    }
}
