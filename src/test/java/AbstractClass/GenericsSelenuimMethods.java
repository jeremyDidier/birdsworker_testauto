package AbstractClass;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.io.File;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public abstract class GenericsSelenuimMethods extends GenericMethods {
    protected WebDriver driver;
    private WebDriverWait wait;


    public GenericsSelenuimMethods(WebDriver driver) {
        this.driver = driver;
    }

    /***
     * Make the explicityWait call easier.
     * The default explicityWait is 10 seconds.
     * @return
     */
    public WebDriverWait explicityWait(){
       return this.wait = new WebDriverWait(driver,10);
    }

    /***
     * Make the explicityWait call easier.
     * We have to specify the number of seconds
     * @return
     */
    public WebDriverWait explicityWait(int seconds){
       return this.wait = new WebDriverWait(driver,seconds);
    }

    public void selectCheckox(WebElement element){
        if(!element.isSelected()){
            element.click();
        }
    }

    public void unselectCheckox(WebElement element){
        if(element.isSelected()){
            element.click();

        }
    }


    public String getSecondWindow(String firstWindow){
        for (String window: driver.getWindowHandles()){
          if(!window.equals(firstWindow)){
              return window;
          }
        }
        return null;
    }

    public boolean isElementDisplayed(WebElement element){
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        try{
            return element.isDisplayed();
        }catch(org.openqa.selenium.NoSuchElementException e){
            return false;
        }
    }

    /***
     * clear localStorage.
     */
    public void clearLocalStorage(){
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("if(window.localStorage.length != 0){localStorage.clear()}");
    }


}
