package AbstractClass;

import Configs.BrowserGetter;
import Configs.ProfileManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public abstract class BaseTest extends GenericMethods {
   protected WebDriver driver;

    /***
     * Before each test suites, get the environnment and the browser.
     * If they aren't specify as a parameter, it only call the data from the configuration file.
     * Otherwise, we write them in the configuration file and then call them from it.
     * @param env
     * @param browser
     */
    @Parameters({"env", "browser"})
    @BeforeClass
    public void beforeTest(@Optional("defaultEnv") String env, @Optional("defaultBrowser") String browser){

        if (!env.equals("defaultEnv")) {
            ProfileManager.configurationWriter("environment", env);
        }
        if (browser.equals("defaultBrowser")) {
            driver = BrowserGetter.getDriver();// setup the driver according to the profile.properties file
        }else{
            driver = BrowserGetter.getDriver(browser);// setup the driver according to the parameter
        }
        System.out.println("***THE DRIVER IS SETUP***");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    /***
     * Use to delete all cookies before each test cases in order to avoid login and navigation conflicts
     */
    @BeforeMethod
    public void beforeEachTest(){
        clearBrowserCookies();
    }

    /***
     * After each test suites, quit the driver.
     */
    @AfterClass
    public void afterTest(){
        driver.quit();
        System.out.println("***THE DRIVER IS CLOSED***");
    }

    /***
     * Return the driver. This method is needed to takescreenshot on TestListeners class.
     * @return
     */
    public WebDriver getDriver(){
        return this.driver;
    }

    /***
     * clear all cookies ( connection and navigation history)
     */
    public void clearBrowserCookies(){
        driver.manage().deleteAllCookies();
        sleep(1);
    }

}
