package Tests;

import AbstractClass.BaseTest;
import Configs.EnvironmentVariables;
import Pages.LoginPage;
import Pages.MailPage;
import Pages.MissionPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class tchating extends BaseTest {
    MailPage mailPage;
    LoginPage loginPage;

    @Test
    public void uploadFile(){
        String emailSalesMan = EnvironmentVariables.getVariable("loginSalesman");
        String password = EnvironmentVariables.getVariable("password");
        mailPage = PageFactory.initElements(driver, MailPage.class);
        loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.signin(emailSalesMan, password);
        mailPage.load();
        mailPage.uploadFile();
    }

    @Test
    public void clickOnLastSentFile(){
        String emailSalesMan = EnvironmentVariables.getVariable("loginSalesman");
        String password = EnvironmentVariables.getVariable("password");
        mailPage = PageFactory.initElements(driver, MailPage.class);
        loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.signin(emailSalesMan, password);
        mailPage.load();
        mailPage.displayAttachmentPage();
        Assert.assertTrue(mailPage.isAttachmentPageDisplayed());
    }

    @Test
    public void sendMessageAsClient(){
        String clientEmail = EnvironmentVariables.getVariable("loginSalesman");
        String password = EnvironmentVariables.getVariable("password");
        String randomMessage = randomString(25);
        mailPage = PageFactory.initElements(driver, MailPage.class);
        loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.signin(clientEmail, password);
        mailPage.load();
        mailPage.sendAMessageAndWaitUntilDisplayed(randomMessage);
        Assert.assertEquals(mailPage.getLastMessage(), randomMessage);
    }

    @Test
    public void sendMessageAsConsultant(){
        String clientEmail = EnvironmentVariables.getVariable("loginConsultant");
        String password = EnvironmentVariables.getVariable("password");
        String randomMessage = randomString(25);
        mailPage = PageFactory.initElements(driver, MailPage.class);
        loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.signin(clientEmail, password);
        mailPage.load();
        mailPage.sendAMessageAndWaitUntilDisplayed(randomMessage);
        Assert.assertEquals(mailPage.getLastMessage(), randomMessage);
    }

    @Test
    public void redirectingToMissionPageFromLasOrder(){
        String clientEmail = EnvironmentVariables.getVariable("loginConsultant");
        String password = EnvironmentVariables.getVariable("password");
        String randomMessage = randomString(25);
        mailPage = PageFactory.initElements(driver, MailPage.class);
        loginPage = PageFactory.initElements(driver, LoginPage.class);
        MissionPage missionPage = PageFactory.initElements(driver, MissionPage.class);
        loginPage.signin(clientEmail, password);
        mailPage.load();
        mailPage.clickOnGoToMissionDetail();
        missionPage.waitMissionPage();
        Assert.assertTrue(missionPage.isMissionDetailsDisplayed());
    }
}

