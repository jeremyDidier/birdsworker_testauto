package Tests;

import AbstractClass.BaseTest;
import Configs.EnvironmentVariables;
import Pages.Header;
import Pages.HomePage;
import Pages.LoginPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class HeaderChecking extends BaseTest {

    @Test
    public void checkLogOutHeader(){
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        Header header = PageFactory.initElements(driver, Header.class);
        homePage.load();
        Assert.assertTrue(header.isLogoDisplayed());
        Assert.assertTrue(header.isLoginDisplayed());
        Assert.assertTrue(header.isRegisterButtonDisplayed());
        Assert.assertFalse(header.isBrowseButtonDisplayed());
        Assert.assertFalse(header.isMessageButtonDisplayed());
        Assert.assertFalse(header.isMissionButtonDisplayed());
        Assert.assertFalse(header.isProfileButtonDisplayed());
        Assert.assertFalse(header.isProfileImageDisplayed());
        Assert.assertFalse(header.isUsersNameDisplayed());
    }

    @Test
    public void checkConsultantHeader(){
        String email = EnvironmentVariables.getVariable("loginConsultant");
        String password = EnvironmentVariables.getVariable("password");

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        Header header = PageFactory.initElements(driver, Header.class);
        loginPage.signin(email, password);

        Assert.assertEquals(header.isLogoDisplayed(), true);
        Assert.assertEquals(header.isLoginDisplayed(), false);
        Assert.assertEquals(header.isRegisterButtonDisplayed(), false);
        Assert.assertEquals(header.isBrowseButtonDisplayed(), true);
        Assert.assertEquals(header.isMessageButtonDisplayed(), true);
        Assert.assertEquals(header.isMissionButtonDisplayed(), true);
        Assert.assertEquals(header.isProfileButtonDisplayed(), true);
        Assert.assertEquals(header.isProfileImageDisplayed(), true);
        Assert.assertEquals(header.isUsersNameDisplayed(), true);
    }

    @Test
    public void checkCustomerHeader(){
        String email = EnvironmentVariables.getVariable("loginSalesman");
        String password = EnvironmentVariables.getVariable("password");

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        Header header = PageFactory.initElements(driver, Header.class);
        loginPage.signin(email, password);

        Assert.assertEquals(header.isLogoDisplayed(), true);
        Assert.assertEquals(header.isLoginDisplayed(), false);
        Assert.assertEquals(header.isRegisterButtonDisplayed(), false);
        Assert.assertEquals(header.isBrowseButtonDisplayed(), true);
        Assert.assertEquals(header.isMessageButtonDisplayed(), true);
        Assert.assertEquals(header.isMissionButtonDisplayed(), true);
        Assert.assertEquals(header.isProfileButtonDisplayed(), true);
        Assert.assertEquals(header.isProfileImageDisplayed(), true);
        Assert.assertEquals(header.isUsersNameDisplayed(), true);
    }
}
