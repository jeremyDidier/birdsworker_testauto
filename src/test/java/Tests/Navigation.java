package Tests;

import AbstractClass.BaseTest;
import Configs.EnvironmentVariables;
import Pages.*;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Navigation extends BaseTest {
    HomePage homePage;
    Header header;
    RegisterPage registerPage;
    LoginPage loginPage;
    CguPage cguPage;
    BrowsePage searchPage;


    @Test(groups={"smokeTest"})
    public void accessWebSite() {
        homePage = PageFactory.initElements(driver, HomePage.class);
        homePage.load();
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, "Birds Worker - trouvez le freelance qu'il vous faut");
    }

    @Test( groups={"smokeTest"})
    public void accessLoginFromHPHeader() {
        homePage = PageFactory.initElements(driver, HomePage.class);
        header = PageFactory.initElements(driver, Header.class);
        loginPage = PageFactory.initElements(driver, LoginPage.class);
        homePage.load();
        header.clickOnLogin();
        loginPage.waitPage();
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, "Login - Birds Worker");
    }

    @Test(groups={"smokeTest"})
    public void accessRegisterFromHPHeader() {
        homePage = PageFactory.initElements(driver, HomePage.class);
        registerPage = PageFactory.initElements(driver, RegisterPage.class);
        homePage.load();
        header.clickOnRegisterButton();
        registerPage.waitRegisterPage();
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, "Register - Birds Worker");
    }

    @Test(priority = 1, groups={"smokeTest"})
    public void accessSearchPage() {
        homePage = PageFactory.initElements(driver, HomePage.class);
        searchPage = PageFactory.initElements(driver, BrowsePage.class);
        homePage.load();
        homePage.searchFromHP("chef", "paris");
        searchPage.waitBrowsePage();
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, "Tous les freelances - Birds Worker");
    }

    @Test(groups={"smokeTest"})
    public void accessRegisterFromHPBody() {
        homePage = PageFactory.initElements(driver, HomePage.class);
        registerPage = PageFactory.initElements(driver, RegisterPage.class);
        homePage.load();
        homePage.clickOnFreelanceRegisterButton();
        registerPage.waitRegisterPage();
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, "Register - Birds Worker");
    }

    @Test(groups={"smokeTest"})
    public void accessRecruiterRegisterFromHPBody() {
        homePage = PageFactory.initElements(driver, HomePage.class);
        registerPage = PageFactory.initElements(driver, RegisterPage.class);
        homePage.load();
        homePage.clickOnRecruiterRegisterButton();
        registerPage.waitRegisterPage();
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, "Register - Birds Worker");
    }

    @Test(groups={"smokeTest"})
    public void accessCGU() {
        homePage = PageFactory.initElements(driver, HomePage.class);
        cguPage = PageFactory.initElements(driver, CguPage.class);
        homePage.load();
        homePage.clickOnCGULink();
        cguPage.waitCguPage();
        String actualTitle = driver.getTitle();
        Assert.assertTrue(actualTitle.contains("CGU"));
    }

    @Test(groups={"smokeTest"})
    public void accessSettings() {
        String login = EnvironmentVariables.getVariable("loginSalesman");
        String password = EnvironmentVariables.getVariable("password");
        header = PageFactory.initElements(driver, Header.class);
        loginPage = PageFactory.initElements(driver, LoginPage.class);

        loginPage.signin(login,password);
        header.clickOnSettingsButton();
        sleep(1);
        Assert.assertTrue(driver.getCurrentUrl().contains("settings"));

    }


}
