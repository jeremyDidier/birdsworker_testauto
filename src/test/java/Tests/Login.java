package Tests;

import AbstractClass.BaseTest;
import Configs.EnvironmentVariables;
import Pages.LoginPage;
import Pages.ProfilePage;
import com.opencsv.CSVReader;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class Login extends BaseTest {

    public List<String[]> csvReader(String pathToCsv){
        try {
            CSVReader reader = new CSVReader(new FileReader(pathToCsv));
            return reader.readAll();
        }catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @DataProvider(name ="logins-CSV-data-providers")
    public Iterator<String[]> loginsFromCSV(){
        return csvReader("./src/test/resources/invalidLogins.csv").iterator();
    }

    @Test
    public void clickOnResetLink(){
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.load();
        loginPage.clickOnResetLink();
        Assert.assertTrue(loginPage.isResetPopinDisplayed());
    }

    @Test(dataProvider = "logins-CSV-data-providers")
    public void controlsInvalidLogins(String email, String password, String error){
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.load();
        loginPage.enterEmail(email);
        loginPage.enterPassword(password);
        loginPage.submitSignIn();
        String errorMessage = loginPage.getErrorMessage();
        System.out.println("Error message : "+ errorMessage);
        Assert.assertTrue(errorMessage.contains(error));
    }

    @Test( enabled = false)
    public void submitReset(){
        String email = EnvironmentVariables.getVariable("loginConsultant");
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.load();
        loginPage.clickOnResetLink();
        loginPage.enterEmailToReset(email);
        loginPage.submitReset();
        Assert.assertTrue(false);
    }

    @Test(enabled = false)
    public void controlReset(){
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.load();
        loginPage.clickOnResetLink();
        loginPage.enterEmailToReset("otgheroghrg@yopmail.com");
        loginPage.submitReset();
        String errorMessage = loginPage.getErrorMessage();
        System.out.println("Error message : "+ errorMessage);
        Assert.assertTrue(errorMessage.contains("existe pas"));
    }

    @Test(priority = 0)
    public void loginAsConsultant(){
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        ProfilePage profilePage = PageFactory.initElements(driver, ProfilePage.class);
        String email = EnvironmentVariables.getVariable("loginConsultant");
        String password = EnvironmentVariables.getVariable("password");
        loginPage.signin(email, password);
        Assert.assertTrue(profilePage.isPageDisplayed());
        Assert.assertFalse(profilePage.isEditPopupDisplayed());
    }

    @Test
    public void loginAsEmptyConsultant(){
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        ProfilePage profilePage = PageFactory.initElements(driver, ProfilePage.class);
        String email = EnvironmentVariables.getVariable("loginEmptyConsultant");
        String password = EnvironmentVariables.getVariable("password");
        loginPage.signin(email, password);
        Assert.assertTrue(profilePage.isEditPopupDisplayed());
    }

    @Test
    public void loginAsAClient(){
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        ProfilePage profilePage = PageFactory.initElements(driver, ProfilePage.class);
        String email = EnvironmentVariables.getVariable("loginSalesman");
        String password = EnvironmentVariables.getVariable("password");
        loginPage.signin(email, password);
        Assert.assertTrue(profilePage.isPageDisplayed());
        Assert.assertFalse(profilePage.isEditPopupDisplayed());
    }



}
