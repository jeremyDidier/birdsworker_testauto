package Tests;

import AbstractClass.BaseTest;
import Pages.BrowsePage;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SearchFromBrowse extends BaseTest {
    private BrowsePage browsePage;

    @Test
    public void getCountResult(){
        browsePage = PageFactory.initElements(driver, BrowsePage.class);
        browsePage.loadBrowsePage();
        browsePage.displayConsultantDetails();
        Assert.assertTrue(browsePage.isConsultantDetailsDisplayed());

    }

    @Test
    public void returnMaxMinSlidderValue(){
        browsePage = PageFactory.initElements(driver, BrowsePage.class);
        browsePage.loadBrowsePage();
        System.out.println(browsePage.getSliderMinValue());
        System.out.println(browsePage.getSliderMaxValue());
        System.out.println(browsePage.getLowerSliderValue());
        System.out.println(browsePage.getUpperSliderValue());
    }
}
