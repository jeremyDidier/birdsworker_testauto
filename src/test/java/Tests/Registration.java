package Tests;

import AbstractClass.BaseTest;
import Configs.EnvironmentVariables;
import Pages.ProfilePage;
import Pages.RegisterPage;
import com.opencsv.CSVReader;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class Registration extends BaseTest {

    public List<String[]> readDataFromCSV(String csvPath){
        try {
            CSVReader reader = new CSVReader(new FileReader(csvPath));
            List<String[]> data = reader.readAll();
            return data;
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @DataProvider(name = "names-CSV-data-providers")
    public Iterator<String[]> namesAndErrorsFromCSV(){
        return readDataFromCSV("./src/test/resources/invalidNames.csv").iterator();
    }

    @DataProvider(name = "logins-passwords-errors-data-provider")
    public Object[][] loginsPasswordsErrorsDP(){
        return new String[][]{
                {"", "", "", "email" },
                {"sgrrgeg", "", "", "e-mail valide" },
                {"sgrrgeg@yopmail", "", "", "e-mail valide" },
                {"sgrrgeg@yopmail.", "", "", "e-mail valide" },
                {"sgrrgeguu@yopmail.com", "78945671", "", "confirmation" },
                {"sgrrgegu@yopmail.com", "Goku3!", "Goku3!", "court" },
                {"sgrrgegt@yopmail.com", "Gokuss03!", "Gokuss04!", "correspondre" },
                {"sgrrgeg@yopmail.com", "Gokuss03!", "Gokuss03!", "existe" },
        };
    }

    @Test
    public void selectCguCheckbox(){
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.load();
        registerPage.selectCguCheckbox();
        Assert.assertTrue(registerPage.isCguSelected());
    }

    @Test
    public void unselectCgu(){
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.load();
        registerPage.selectCguCheckbox();
        registerPage.unselectCguCheckbox();
        Assert.assertFalse(registerPage.isCguSelected());
    }

    @Test
    public void displayCgu(){
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.load();
        registerPage.displayCgu();
        Assert.assertTrue(registerPage.isCguDisplay());
    }

    @Test(dataProvider = "logins-passwords-errors-data-provider")
    public void controlsOfRegistrationFailures(String login, String password, String confirmation, String errorMessage){
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.load();
        registerPage.enterEmail(login);
        registerPage.enterPassword(password);
        registerPage.enterPasswordConfirmation(confirmation);
        registerPage.submitRegistration();
        System.out.println("Error message = "+registerPage.getErrorMessage());
        Assert.assertTrue(registerPage.getErrorMessage().contains(errorMessage));
    }

    @Test
    public void controlsOfExistingEmail(){
        String existingEmail = EnvironmentVariables.getVariable("loginConsultant");
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.load();
        registerPage.enterEmail(existingEmail);
        registerPage.enterPassword("Gokuss03!");
        registerPage.enterPasswordConfirmation("Gokuss03!");
        registerPage.selectCguCheckbox();
        registerPage.submitRegistration();
        Assert.assertTrue(registerPage.getErrorMessage().contains("compte existe"));
    }

    @Test
    public void submitFirstRegisterForm(){
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.validateStep1();
        Assert.assertTrue(registerPage.isStep2Displays());
    }

    @Test
    public void selectCguOnStep2(){
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.validateStep1();
        registerPage.selectCguCheckbox();
        Assert.assertTrue(registerPage.isCguSelected());
    }

    @Test
    public void unselectCguOnStep2(){
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.validateStep1();
        registerPage.selectCguCheckbox();
        registerPage.unselectCguCheckbox();
        Assert.assertFalse(registerPage.isCguSelected());
    }

    @Test
    public void displayCguOnStep2(){
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.validateStep1();
        registerPage.displayCgu();
        Assert.assertTrue(registerPage.isCguDisplay());
    }

    @Test
    public void selectClientOption(){
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.validateStep1();
        registerPage.selectClient();
        registerPage.isClientSelected();
    }

    @Test
    public void selectFreelanceOption(){
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.validateStep1();
        registerPage.selectFreelance();
        registerPage.isFreelanceSelected();
    }

    @Test
    public void validEmptyStep2(){
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.validateStep1();
        registerPage.submitStep2();
        Assert.assertTrue(registerPage.getErrorMessage().contains("profil"));
    }


    @Test(dataProvider = "names-CSV-data-providers")
    public void namesCharactersControls(String firstname, String lastname, String errorMessage){
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.validateStep1();
        registerPage.selectClient();
        registerPage.selectCguCheckbox();
        registerPage.enterFirstName(firstname);
        registerPage.enterLastName(lastname);
        registerPage.submitStep2();
        Assert.assertTrue(registerPage.getErrorMessage().contains(errorMessage));
    }

    @Test
    public void createFreelance(){
        String firstname = randomString(8);
        String lastname = randomString(8);
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        ProfilePage profilePage = PageFactory.initElements(driver, ProfilePage.class);
        registerPage.validateStep1();
        registerPage.selectFreelance();
        registerPage.enterFirstName(firstname);
        registerPage.enterLastName(lastname);
        registerPage.selectCguCheckbox();
        registerPage.submitStep2();
        profilePage.waitPage();
        Assert.assertTrue(profilePage.isEditPopupDisplayed());
    }

    @Test
    public void createCustomer(){
        String firstname = randomString(8);
        String lastname = randomString(8);
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        ProfilePage profilePage = PageFactory.initElements(driver, ProfilePage.class);
        registerPage.validateStep1();
        registerPage.selectClient();
        registerPage.enterFirstName(firstname);
        registerPage.enterLastName(lastname);
        registerPage.selectCguCheckbox();
        registerPage.submitStep2();
        profilePage.waitPage();
        Assert.assertTrue(profilePage.isPageDisplayed());
    }


}
