package Pages;

import AbstractClass.GenericsSelenuimMethods;
import Configs.EnvironmentVariables;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CguPage extends GenericsSelenuimMethods {

    @FindBy(css = ".c4.c33")
    private WebElement cguTitle;

    private String url = EnvironmentVariables.getVariable("url");// Get the url from the right environment

    public CguPage(WebDriver driver) {
        super(driver);
    }

    public void loadCguPage(){
        driver.get(url+"cgu");
        explicityWait().until(ExpectedConditions.visibilityOf(cguTitle));
    }

    public void waitCguPage(){
        explicityWait().until(ExpectedConditions.visibilityOf(cguTitle));
    }

}
