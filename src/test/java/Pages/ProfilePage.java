package Pages;

import AbstractClass.GenericsSelenuimMethods;
import Configs.EnvironmentVariables;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

public class ProfilePage extends GenericsSelenuimMethods {

    @FindBy(className = "my-profile")
    private WebElement profileForm;


    // Header
    @FindBy(css =".profile-head-content .right")
    private WebElement headerName;

    @FindBy(className ="profile-picture ")
    private WebElement profilePicture;

    @FindBy(className ="edit-headline")
    private WebElement editButton;

    @FindBy(className ="price")
    private WebElement headerPrice;

    @FindBy(className ="experience")
    private WebElement headerExperience;

    @FindBy(className ="my-profile-not-ready-warning-banner")
    private WebElement bannerWarning;

    @FindBy(css =".my-profile-not-ready-warning-banner>.button")
    private WebElement bannerEditButton;


    // Form
    @FindBy(className ="i-am-available")
    private WebElement checkBoxAvailability;

    @FindBy(className ="edit-location")
    private WebElement editLocationButton;

    @FindBy(css =".checkbox-mission-type-office > input")
    private WebElement checkBoxOffice;

    @FindBy(css =".checkbox-mission-type-remote > input")
    private WebElement checkBoxRemote;

    @FindBy(css ="div[onclick='editPresentation()']")
    private WebElement editPresentationButton;

    @FindBy(css =".section-skills pre")
    private WebElement presentationText;

    @FindBy(className ="new-tag")
    private WebElement editTag;

    @FindBy(className ="new-tag")
    private List<WebElement>  choosenTag;

    @FindBy(className ="years-of-xp")
    private WebElement expSelection;

    @FindBy(className ="add-experience")
    private WebElement addExpButton;

    @FindBy(className ="experiences-list")
    private WebElement expListSection;

    @FindBy(className ="experiences-item")
    private List<WebElement> expItems;

    @FindBy(className ="action-button--edit")
    private List<WebElement> editExpPicto;

    @FindBy(className ="action-button--delete")
    private List<WebElement> deleteExpPicto;

    @FindBy(xpath ="//div[@class='section-title'][descendant::img]/following-sibling::div[1]")
    private WebElement sectionReferences;


    // Popup edit profil
    @FindBy(className ="app-popup")
    private WebElement popupEditProfil;

    @FindBy(className ="app-popup-header-close")
    private WebElement closeEditProfilButton;

    @FindBy(className ="firstname")
    private WebElement firstNameField;

    @FindBy(className ="lastname")
    private WebElement lastNameField;

    @FindBy(css ="input[class='headline']")
    private WebElement titleField;

    @FindBy(css ="input[class='price']")
    private WebElement dailyRateField;

    @FindBy(css ="button[type='submit']")
    private WebElement submitEditButton;


    // popin location
    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]")
    private WebElement popinLocation;

    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]//img")
    private WebElement popinLocationCloseButton;

    @FindBy(className ="pac-target-input")
    private WebElement locationField;

    @FindBy(className ="pac-item")
    private List<WebElement> autoCompletionValues;

    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]//div[contains(@class,'button')]")
    private WebElement locationRegistrationButton;


    // Error popin
    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][2]")
    private WebElement popinError;

    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][2]//img")
    private WebElement popinErrorCloseButton;

    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][2]//div[contains(@class,'button')]")
    private WebElement popinErrorOkButton;


    // Popin edit presentation
    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]")
    private WebElement popinEditPresentation;

    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]//img")
    private WebElement popinEditPresentationCloseButton;

    @FindBy(className ="description")
    private WebElement descriptionField;

    @FindBy(className ="validate")
    private WebElement descriptionValidateButton;


    // Popin edit Tag
    @FindBy(css ="edit-tags-popup")
    private WebElement popinEditTag;

    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]//img")
    private WebElement popinTagcloseButton;

    @FindBy(className ="search-tags")
    private WebElement tagField;

    @FindBy(css=".tag .left")
    private List<WebElement> tagName;

    @FindBy(css=".tag .add-button")
    private List<WebElement> addATagButton;

    @FindBy(css=".add-button:not(.right)")
    private WebElement addANewTagButton;


    // Popup confirm deteling
    @FindBy(className ="popup-confirm")
    private WebElement popupConfirmDeleting;

    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]//img")
    private WebElement popinConfirmCloseButton;

    @FindBy(css=".popup-confirm .delete")
    private WebElement deleteButton;

    @FindBy(css=".popup-confirm .cancel")
    private WebElement cancelDeletion;


    // Popin add Experience
    @FindBy(className ="manage-experience-popup")
    private WebElement popinEditExperience;

    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]//img")
    private WebElement popinExperienceCloseButton;

    @FindBy(className ="title")
    private WebElement nameOfClientfield;

    @FindBy(className ="year")
    private WebElement yearOfMissionField;

    @FindBy(className ="duration")
    private WebElement durationOfMissionField;

    @FindBy(className ="description")
    private WebElement descriptionOfMissionField;

    @FindBy(className ="validate")
    private WebElement validateExperienceButton;




    private String url = EnvironmentVariables.getVariable("url");// Get the url from the right environment

    
    public ProfilePage(WebDriver driver) {
        super(driver);
    }

    public void load(){
        driver.get(url+"my-profile");
        explicityWait().until(ExpectedConditions.visibilityOf(profileForm));
    }

    public void waitPage(){
        explicityWait().until(ExpectedConditions.visibilityOf(profileForm));
    }

    public boolean isPageDisplayed(){
        return profileForm.isDisplayed();
    }

    public String getNames(){
        return headerName.getText();
    }

    public void clickOnPicture(){
        profilePicture.click();
    }

    public void clickOnEdit(){
        editButton.click();
        waitForEditPopup();
    }

    public void waitForEditPopup(){
        explicityWait().until(ExpectedConditions.visibilityOf(popupEditProfil));
    }

    public boolean isEditPopupDisplayed(){
        return isElementDisplayed(popupEditProfil);
    }

    public void getHeaderDailyRate(){
        headerPrice.getText();
    }

    public void getHeaderExperience(){
        headerExperience.getText();
    }

    public boolean isBannerDisplay(){
       return bannerWarning.isDisplayed();
    }

    public void clickOnEditBanner(){
         bannerEditButton.click();
    }

    public void selectAvailability(){
        selectCheckox(checkBoxAvailability);
    }

    public void unselectAvailability(){
        unselectCheckox(checkBoxAvailability);
    }

    public void clickOnLocationButton(){
        editLocationButton.click();
    }

    public void displayLocationPopin(){
        clickOnLocationButton();
        explicityWait().until(ExpectedConditions.visibilityOf(popinLocation));
    }

    public boolean isPopinLocationDisplayed(){
        return popinLocation.isDisplayed();
    }

    public void closePopinLocation(){
        popinLocationCloseButton.click();
    }

    public void enterLocation(String location){
        locationField.click();
    }

    public void registerLocation(){
        locationRegistrationButton.click();
    }

    public boolean isLocationAutocompletionEmpty(){
        return autoCompletionValues.isEmpty();
    }

    public void selectWorkOffice(){
        selectCheckox(checkBoxOffice);
    }

    public void unselectWorkOffice(){
        unselectCheckox(checkBoxOffice);
    }

    public void selectWorkRemote(){
        selectCheckox(checkBoxRemote);
    }

    public void unselectWorkRemote(){
        unselectCheckox(checkBoxRemote);
    }

    public void clickOnEditPresentationButton(){
        editPresentationButton.click();
    }

    public void displayPresentationPopin(){
        clickOnEditPresentationButton();
        explicityWait().until(ExpectedConditions.visibilityOf(popinEditPresentation));
    }

    public boolean isPresentationPopinDisplay(){
        displayPresentationPopin();
        return popinEditPresentation.isDisplayed();
    }

    public void closePresentationPopin(){
        popinEditPresentationCloseButton.click();
    }

    public void enterDescription(String description){
        descriptionField.clear();
        descriptionField.sendKeys(description);
    }

    public void validateDescription(){
        descriptionValidateButton.click();
    }

    public String getPresentationText(){
        return presentationText.getText();
    }

    public void clickOnAddaSkill(){
        editTag.click();
    }

    public void displayTagPopin(){
        clickOnAddaSkill();
        explicityWait().until(ExpectedConditions.visibilityOf(popinEditTag));
    }

    public boolean isTagPopinDisplay(){
        return popinEditTag.isDisplayed();
    }

    public void closeTagPopin(){
        popinTagcloseButton.click();
    }

    public void enterTag(String tag){
        tagField.clear();
        tagField.sendKeys(tag);
    }

    public ArrayList<String> getTagNames(){
        ArrayList<String> tagNames = new ArrayList<>();
        for (WebElement tag: tagName){
            tagNames.add(tag.getText());
        }
        return tagNames;
    }

    public void addTag(int index){
        addATagButton.get(index).click();
    }

    public boolean isNewTagButtonDisplayed(){
        explicityWait().until(ExpectedConditions.visibilityOf(addANewTagButton));
        return addANewTagButton.isDisplayed();
    }

    public void clickOnAddAnewtag(){
        explicityWait().until(ExpectedConditions.visibilityOf(addANewTagButton));
        addANewTagButton.click();
    }

    public boolean isChoosenTagdisplayed(String tagName){
        explicityWait().until(ExpectedConditions.visibilityOf(addANewTagButton));
        for (WebElement tag: choosenTag){
            if(tag.getText().equals(tagName)){
                return true;
            }
        }return false;
    }

    public void clickOnChoosenTag(String tagName){
        explicityWait().until(ExpectedConditions.visibilityOf(addANewTagButton));
        for (WebElement tag: choosenTag){
            if(tag.getText().equals(tagName)){
                tag.click();
            }
        }
    }

    public boolean isDeleteTagPopinDisplayed(){
        explicityWait().until(ExpectedConditions.visibilityOf(popupConfirmDeleting));
        return popupConfirmDeleting.isDisplayed();
    }
    
    public void closeDeleteTagPopin(){
        popinConfirmCloseButton.click();
    }

    public void deleteTag(){
        deleteButton.click();
    }

    public void cancelTagDeletion(){
        cancelDeletion.click();
    }

    public void selectYearsOfExp(String years){
        Select select = new Select(expSelection);
        select.selectByVisibleText(years);
    }

    public boolean isYearsOfExpSelected(String years){
        Select select = new Select(expSelection);
        return select.getFirstSelectedOption().getText().contains(years);
    }

    public void displayExpPopin(){
        addExpButton.click();
        explicityWait().until(ExpectedConditions.visibilityOf(popinEditExperience));
    }

    public boolean isExpPopinDisplayed(){
        try {
            return popinEditExperience.isDisplayed();
        }catch(org.openqa.selenium.NoSuchElementException e){
            return false;
        }
    }

    public void closeExpPopin(){
        popinExperienceCloseButton.click();
    }

    public void enterClientsName(String clientsName){
        nameOfClientfield.clear();
        nameOfClientfield.sendKeys(clientsName);
    }

    public void enterYearOfMission(String yearOfMission){
        yearOfMissionField.clear();
        yearOfMissionField.sendKeys(yearOfMission);
    }

    public void enterDurationOfMission(String durationOfMission){
        durationOfMissionField.clear();
        durationOfMissionField.sendKeys(durationOfMission);
    }

    public void enterDescirptionOfMission(String descriptionOfMission){
        descriptionOfMissionField.clear();
        descriptionOfMissionField.sendKeys(descriptionOfMission);
    }

    public void validateNewExp(){
        validateExperienceButton.click();
    }

    public boolean isExperienceListPresent(){
        return expListSection.isDisplayed();
    }

    public int getNumberOfExperiences(){
        return expItems.size();
    }


}
