package Pages;

import AbstractClass.GenericsSelenuimMethods;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Header extends GenericsSelenuimMethods {

    // Header when user is not connected
    @FindBy(css= "img[class*='top-logo']")
    private WebElement logo;

    @FindBy(css = "*[onclick*='login']")
    private WebElement headerLoginButton;

    @FindBy(css = "*[onclick*='register']")
    private WebElement headerRegisterButton;

    // When connected as sales man
    @FindBy(css = ".browse-top-menu-left *[href*=browse]")
    private WebElement browseButton;

    @FindBy(css = ".browse-top-menu-left *[href*=profile]")
    private WebElement profileButton;

    @FindBy(css = ".browse-top-menu-left *[href*=missions]")
    private WebElement missionButton;

    @FindBy(css = ".browse-top-menu-right *[href*=conversations]")
    private WebElement messageButton;

    @FindBy(css = ".user-actions .name")
    private WebElement nameOnHeader;

    @FindBy(css = ".user-actions img[src*=profile]")
    private WebElement profileImage;

    @FindBy(css = ".user-actions *[href*=settings]")
    private WebElement settingsButton;

    @FindBy(css = ".user-actions *[href*=logout]")
    private WebElement logoutButton;


    public Header(WebDriver driver) {
        super(driver);
    }

    public boolean isLogoDisplayed(){
        return isElementDisplayed(logo);
    }

    public void clickOnLogo(){
        logo.click();
    }

    public boolean isLoginDisplayed(){
        return isElementDisplayed(headerLoginButton);
    }

    public void clickOnLogin(){
        headerLoginButton.click();
    }

    public boolean isRegisterButtonDisplayed(){
        return isElementDisplayed(headerRegisterButton);
    }

    public void clickOnRegisterButton() {
        headerRegisterButton.click();
    }

    public boolean isBrowseButtonDisplayed(){
        return isElementDisplayed(browseButton);
    }

    public void clickOnBrowseButton() {
        browseButton.click();
    }

    public boolean isProfileButtonDisplayed(){
        return isElementDisplayed(profileButton);
    }

    public void clickOnProfileButton() {
        profileButton.click();
    }

    public boolean isMissionButtonDisplayed(){
        return isElementDisplayed(missionButton);
    }

    public void clickOnMissionButton() {
        missionButton.click();
    }

    public boolean isMessageButtonDisplayed(){
        return isElementDisplayed(messageButton);
    }

    public void clickOnMessageButton() {
        messageButton.click();
    }

    public boolean isUsersNameDisplayed(){
        return isElementDisplayed(nameOnHeader);
    }

    public String getName() {
        return nameOnHeader.getText();
    }

    public boolean isProfileImageDisplayed(){
        return isElementDisplayed(profileImage);
    }

    public void clickOnProfileImage() {
        profileImage.click();
    }

    public void clickOnSettingsButton() {
        clickOnProfileImage();
        settingsButton.click();
    }

    public void clickOnLogoutButton() {
        clickOnProfileImage();
        logoutButton.click();
    }

}
