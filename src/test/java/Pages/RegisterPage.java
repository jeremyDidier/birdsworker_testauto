package Pages;


import AbstractClass.GenericsSelenuimMethods;
import Configs.EnvironmentVariables;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class RegisterPage extends GenericsSelenuimMethods {


    @FindBy(className = "register-page-step1")
    private WebElement registerFormBlock;

    @FindBy(className = "linkedin-button")
    private WebElement linkedinLink;

    @FindBy(className = "email")
    private WebElement emailField;

    @FindBy(className = "password")
    private WebElement passwordField;

    @FindBy(className = "passwordVerification")
    private WebElement passwordVerifField;

    @FindBy(className = "cgu")
    private WebElement cguCheckbox;

    @FindBy(className = "open-cgu")
    private WebElement cguOpenLink;

    @FindBy(className = "button")
    private WebElement submitButton;

    // Error Popin
    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]")
    private WebElement errorPopin;

    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]//img")
    private WebElement closeButtonOfErrorPopin;

    @FindBy(xpath ="(//div[contains(@class, 'app-popup--show')][1]//div[@style])[1]")
    private WebElement contentOfErrorPopin;

    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]//div[contains(@class, 'button')]")
    private WebElement okButtonOfErrorPopin;

    // Register step 2
    @FindBy(className = "profileTypes")
    private WebElement resgisterStep2;

    @FindBy(xpath = "//div[@data-type ='client']")
    private WebElement client;

    @FindBy(xpath = "//div[@data-type ='freelance']")
    private WebElement freelance;

    @FindBy(className="firstname")
    private WebElement firstNameField;

    @FindBy(className="lastname")
    private WebElement lastNameField;

    @FindBy(css = ".button.validate")
    private WebElement validateButton;



    private String url = EnvironmentVariables.getVariable("url");
    public RegisterPage(WebDriver driver) {
        super(driver);
    }


    public void load(){
        driver.get(url+"register");
        explicityWait().until(ExpectedConditions.visibilityOf(registerFormBlock));
    }

    public void waitRegisterPage(){
        explicityWait().until(ExpectedConditions.visibilityOf(registerFormBlock));
    }

    public void clickOnLinkedinLink(){
        linkedinLink.click();
    }

    public void enterEmail(String email){
        emailField.sendKeys(email);
    }

    public void enterPassword(String password){
        passwordField.sendKeys(password);
    }

    public void enterPasswordConfirmation(String passwordConfirmation){
        passwordVerifField.sendKeys(passwordConfirmation);
    }

    public void selectCguCheckbox(){
        if(!cguCheckbox.isSelected()) {
            cguCheckbox.click();
        }
    }

    public boolean isCguSelected(){
        return cguCheckbox.isSelected();
    }
    public void unselectCguCheckbox(){
        if(cguCheckbox.isSelected()) {
            cguCheckbox.click();
        }
    }

    public void displayCgu(){
        cguOpenLink.click();
    }

    public boolean isCguDisplay(){
         explicityWait().until(ExpectedConditions.titleIs("CGU | Birds Worker"));
        return driver.getTitle().equals("CGU | Birds Worker");
    }

    public void submitRegistration(){
        submitButton.click();
    }

    public String getErrorMessage(){
        explicityWait().until(ExpectedConditions.visibilityOf(errorPopin));
        return contentOfErrorPopin.getText();
    }

    public void validErrorPopin(){
        okButtonOfErrorPopin.click();
        explicityWait(1).until(ExpectedConditions.invisibilityOf(errorPopin));
    }

    public void waitForStep2(){
        explicityWait(1).until(ExpectedConditions.visibilityOf(resgisterStep2));
    }

    public boolean isStep2Displays(){
        return driver.getCurrentUrl().contains("register_linkedin_final");
    }

    public void validateStep1(){
        String email = randomString(10)+"@yopmail.com";
        load();
        enterEmail(email);
        enterPassword("Gokuss03!");
        enterPasswordConfirmation("Gokuss03!");
        selectCguCheckbox();
        submitRegistration();
        waitForStep2();
    }

    public void selectClient(){
        client.click();
    }

    public boolean isClientSelected(){
        return client.isSelected();
    }

    public void selectFreelance(){
        freelance.click();
    }
    public boolean isFreelanceSelected(){
        return freelance.isSelected();
    }

    public void enterFirstName(String firstName){
        firstNameField.clear();
        firstNameField.sendKeys(firstName);
    }

    public void enterLastName(String lastName){
        lastNameField.clear();
        lastNameField.sendKeys(lastName);
    }

    public void submitStep2(){
        validateButton.click();
    }



}
