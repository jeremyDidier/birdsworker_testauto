package Pages;


import AbstractClass.GenericsSelenuimMethods;
import Configs.EnvironmentVariables;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class SettingPage extends GenericsSelenuimMethods {

    @FindBy (className = "my-settings")
    private WebElement settingsBlock;

    @FindBy (className = "checkbox-allow-marketing-email")
    private WebElement newsletterCheckbox;

    @FindBy (className = "checkbox-activate-account")
    private WebElement activateAccountCheckbox;

    @FindBy (className = "change-password")
    private WebElement changePasswordButton;

    // Popin confirmation of  reset email sent
    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]")
    private WebElement popinResetPassword;

    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]//img")
    private WebElement popinResetPasswordCloseButton;

    @FindBy(className ="//div[contains(@class, 'app-popup--show')][1]//div[contains(@class, 'button')]")
    private WebElement okButton;



    private String url = EnvironmentVariables.getVariable("url");

    public SettingPage(WebDriver driver) {
        super(driver);
    }

    public void loadSettingsPage(){
        driver.get(url+"my-settings");
        explicityWait().until(ExpectedConditions.visibilityOf(settingsBlock));
    }

    public void waitForSettingsPageLoading(){
        explicityWait().until(ExpectedConditions.visibilityOf(settingsBlock));
    }

    public void activateNewsLetter(){
        selectCheckox(newsletterCheckbox);
    }

    public void disableNewsLetter(){
        unselectCheckox(newsletterCheckbox);
    }

    public boolean isNewsletterActivated(){
        return newsletterCheckbox.isSelected();
    }

    public void activateAccountCheckbox(){
        selectCheckox(activateAccountCheckbox);
    }

    public void disableAccountCheckbox(){
        unselectCheckox(activateAccountCheckbox);
    }

    public boolean isAccountCheckboxActivated(){
        return activateAccountCheckbox.isSelected();
    }

    public void clickOnChangePassword(){
        changePasswordButton.click();
    }

    public void waitForResetConfirmationPopin(){
        explicityWait().until(ExpectedConditions.visibilityOf(popinResetPassword));
    }

    public boolean isResetConfirmationPopinDisplayed(){
        return popinResetPassword.isDisplayed();
    }

    public void closeResetPopin(){
        popinResetPasswordCloseButton.click();
    }

    public void confirmResetPopin(){
        okButton.click();
    }



}
