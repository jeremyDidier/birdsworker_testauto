package Pages;

import AbstractClass.GenericsSelenuimMethods;
import Configs.EnvironmentVariables;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;

public class MailPage extends GenericsSelenuimMethods {


    @FindBy(className = "conversations-page")
    private WebElement convertionPage;

    // Empty page
    @FindBy(className = "no-conversations-message")
    private WebElement emptyMessageAlert;

    @FindBy(css = ".no-conversations-message a")
    private WebElement redirToBrowseButton;


    // Not empty page

    //Left side
    @FindBy(css = ".left-list .name")
    private WebElement previousDiscussionName;

    @FindBy(className = "last-message-date")
    private WebElement lastMessageDate;

    @FindBy(className = "last-message-preview")
    private WebElement previewMessage;

    @FindBy(className = "left-list-help")
    private WebElement helpMessage;

    // Right side
    @FindBy(className = "title")
    private WebElement consutantOfSelectedDisc;

    @FindBy(css = ".headline .headline")
    private WebElement ConsultantJobTitle;

    @FindBy(className = "message--right")
    private List<WebElement> allSentMessages;

    @FindBy(xpath = "(//div[contains(@class, 'message-content')])[last()]")
    private WebElement lastMessage;

    @FindBy(xpath = "//div[contains(@class, 'messages')]//descendant::div[@class= 'order'][last()]")
    private WebElement lastOrder;

    @FindBy(xpath = "(//div[contains(@class, 'message-content--file')])[last()]")
    private WebElement lastFile;

    @FindBy(className = "order-title")
    private WebElement lastOrderTitle;

    @FindBy(className = "order-days")
    private WebElement lastOrderDay;

    @FindBy(className = "order-start")
    private WebElement firstDay;

    @FindBy(className = "open-quick-detail-popup")
    private WebElement detailButton;

    @FindBy(className = "answer")
    private WebElement orderAnswer;

    @FindBy(className = "goto-mission-details")
    private WebElement goToMissionDetail;


    // Bottom of page
    @FindBy(className = "order-button")
    private WebElement orderButton;

    @FindBy(className = "send-file-button")
    private WebElement sendFileButton;

    @FindBy(tagName = "textarea")
    private WebElement inputMessage;

    @FindBy(className = "send-message")
    private WebElement sendMessage;


    // Proposal mission popin
    @FindBy(className = "proposal-mission-popup")
    private WebElement proposalMissionPopin;

    @FindBy(className = "app-popup-header-close")
    private WebElement closeProposalPopin;

    @FindBy(className = "app-popup-header-title")
    private WebElement ProposalPopinTitle;

    @FindBy(className = "form-name")
    private WebElement proposedMissionTitle;

    @FindBy(className = "description")
    private WebElement proposedMissionDescription;

    @FindBy(className = "form-days")
    private WebElement proposedMissionDuration;

    @FindBy(className = "form-date")
    private WebElement proposedMissionStartDate;

    @FindBy(className = "validate")
    private WebElement validateMissionProposal;

    @FindBy(className = "popup-payment")
    private WebElement paymentPopup;

    @FindBy(css = ".iban-payment .checkbox")
    private WebElement ibanPaymentCheckBox;

    @FindBy(css = ".card-payment .checkbox")
    private WebElement cardPaymentCheckBox;


    // Error popin
    @FindBy(xpath = "//div[contains(@class, 'app-popup--show')][2]")
    private WebElement popinError;

    @FindBy(xpath = "//div[contains(@class, 'app-popup--show')][2]//img")
    private WebElement popinErrorCloseButton;

    @FindBy(xpath = "//div[contains(@class, 'app-popup--show')][2]//div[contains(@class,'button')]")
    private WebElement popinErrorOkButton;


    // Proposal mission popin
    @FindBy(className = ".app-popup-content div[style]")
    private WebElement recapDescriptionPopin;

    @FindBy(className = "app-popup-header-close")
    private WebElement closeRecapPopin;


    private String url = EnvironmentVariables.getVariable("url");// Get the url from the right environment

    public MailPage(WebDriver driver) {
        super(driver);
    }

    public void load() {
        driver.get(url + "conversations");
        explicityWait().until(ExpectedConditions.visibilityOf(sendMessage));
    }

    public void waitMailPage() {
        explicityWait().until(ExpectedConditions.visibilityOf(sendMessage));
    }

    // Action in empty Message Page
    public boolean isEmptyMessageDisplayed() {
        return emptyMessageAlert.isDisplayed();
    }

    public void clickOnRedirectToBrowse() {
        redirToBrowseButton.click();
    }


    // Action in an not empty Message Page

    // Left side
    public String getPreviousDiscussionName() {
        return previousDiscussionName.getText();
    }

    public String getLastMessageDate() {
        return lastMessageDate.getText();
    }

    public String getPreviewMessage() {
        return previewMessage.getText();
    }

    public boolean isHelpMessageDisplayed() {
        return helpMessage.isDisplayed();
    }

    public String getMainDiscussionName() {
        return consutantOfSelectedDisc.getText();
    }

    public String getMainDiscussionJobTitle() {
        return ConsultantJobTitle.getText();
    }

    public String getLastMessage() {
        return lastMessage.getText();
    }

    public int getNumberOfSentMassages(){
        return allSentMessages.size();
    }

    public String getTitleLastOrder() {
        return lastOrderTitle.getText();
    }

    public String getTitleLastOrderDuration() {
        return lastOrderDay.getText();
    }

    public String getLastOrderStartDate() {
        String[] allText = firstDay.getText().split(" ");
        return allText[allText.length - 1];
    }

    public void clickOnLastFile(){
        lastFile.click();
    }
    
    public void displayAttachmentPage(){
        String firstWindow = driver.getWindowHandle();
        clickOnLastFile();
        explicityWait().until(ExpectedConditions.numberOfWindowsToBe(2));
        String secondWindow = getSecondWindow(firstWindow);
        driver.switchTo().window(secondWindow);
    }

    public boolean isAttachmentPageDisplayed(){
        return driver.getCurrentUrl().equals(url+"api/document/me/pj.png");
    }

    public void displayLastOrderDetails() {
        detailButton.click();
        explicityWait().until(ExpectedConditions.visibilityOf(recapDescriptionPopin));
    }

    public boolean isLastOrderDetailsDisplayed() {
        return recapDescriptionPopin.isDisplayed();
    }

    public String getLastOrderDetailsText() {
        return recapDescriptionPopin.getText();
    }

    public void closeOrderDetailsPopin() {
        closeRecapPopin.click();
    }

    public void clickOnGoToMissionDetail() {
        goToMissionDetail.click();
    }

    public void displayProposalMission() {
        orderButton.click();
        explicityWait().until(ExpectedConditions.visibilityOf(proposalMissionPopin));
    }

    public boolean isProposalMissionPopinDisplayed() {
        return proposalMissionPopin.isDisplayed();
    }

    public void closeProposalMissionPopin() {
        closeProposalPopin.click();
    }

    public String getProposalPopinTitle() {
        return ProposalPopinTitle.getText();
    }

    public void enterProposedMissionTitle(String title) {
        proposedMissionTitle.clear();
        proposedMissionTitle.sendKeys(title);
    }

    public void enterProposedMissionDescription(String description) {
        proposedMissionDescription.clear();
        proposedMissionDescription.sendKeys(description);
    }

    public void enterProposedMissionDuration(String duration) {
        proposedMissionDuration.clear();
        proposedMissionDuration.sendKeys(duration);
    }

    public void enterStartdate(String jjmmyyyy) {
        proposedMissionStartDate.clear();
        proposedMissionStartDate.sendKeys(jjmmyyyy);
    }

    public void validationOfProposal() {
        validateMissionProposal.click();
    }

    public void waitForPaymentPopup() {
        explicityWait().until(ExpectedConditions.visibilityOf(paymentPopup));
    }

    public void clickOnIban() {
        ibanPaymentCheckBox.click();
    }

    public boolean isIbanSelected() {
        return ibanPaymentCheckBox.isSelected();
    }

    public void clickOnCB() {
        cardPaymentCheckBox.click();
    }

    public boolean isCBSelected() {
        return cardPaymentCheckBox.isSelected();
    }

    public void uploadFile() {

        sendFileButton.click();
        sleep(1);
        String absolutePath = getPath("PJ/pj.png");
        System.out.println(absolutePath);
        StringSelection stringSelection = new StringSelection(absolutePath);
        Robot robot = null;
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyRelease(KeyEvent.VK_V);
        sleep(1);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        sleep(1);
    }

    public void enterAMessage(String message){
        inputMessage.clear();
        inputMessage.sendKeys(message);
    }

    public void clickOnSendMessage(){
        sendMessage.click();
    }

    public void sendAMessageAndWaitUntilDisplayed(String message){
        int numberOfSentMessages = getNumberOfSentMassages();
        enterAMessage(message);
        clickOnSendMessage();
        explicityWait().until(ExpectedConditions.numberOfElementsToBe(By.className("message--right"),numberOfSentMessages+1));
    }

}
