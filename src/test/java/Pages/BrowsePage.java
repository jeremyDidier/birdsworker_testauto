package Pages;

import AbstractClass.GenericsSelenuimMethods;
import Configs.EnvironmentVariables;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BrowsePage extends GenericsSelenuimMethods {


    //Header
    @FindBy(className = "searchQuery")
    private WebElement inputSearch;


    //Filters on the left side
    @FindBy(className = "location")
    private WebElement inputLocation;

    @FindBy(className = "acceptOfficeWork")
    private WebElement checkboxOfficeWork;

    @FindBy(className = "acceptRemoteWork")
    private WebElement checkboxRemoteWork;

    @FindBy(className = "noUi-handle-lower")
    private WebElement lowerDailyRateSlider;

    @FindBy(css = ".noUi-handle-lower .noUi-tooltip")
    private WebElement lowerDailyRateValue;

    @FindBy(className = "noUi-handle-upper")
    private WebElement upperDailyRateSlider;

    @FindBy(css = ".noUi-handle-upper .noUi-tooltip")
    private WebElement upperDailyRateValue;

    @FindBy(css = ".checkbox-filter.keywords")
    private WebElement keywordsBlock;

    @FindBy(css = ".checkbox-filter.keywords input")
    private List<WebElement> keywords;


    // Body result
    @FindBy(className = "browse-page-content")
    private WebElement resultsBlock;

    @FindBy(className = "results-count")
    private WebElement resultsCount;

    @FindBy(className = "browse-results-consultant")
    private List<WebElement> consultantBloks;

    @FindBy(className = "name")
    private List<WebElement> consultantsName;

    @FindBy(className = "headline")
    private List<WebElement> consultantsTitle;

    @FindBy(className = "price")
    private List<WebElement> consultantsDailyRate;

    @FindBy(className = "experience")
    private List<WebElement> consultantsExperience;

    @FindBy(className = "tag")
    private List<WebElement> consultantsKeywords;


    //Popup consultant's Details
    @FindBy(className = "app-popup--show")
    private WebElement popupConsultantDetails;

    @FindBy(className = "app-popup-header-close")
    private WebElement closeConsultantDetails;

    @FindBy(className = "profile-headline-name")
    private WebElement consultantDetailsName;

    @FindBy(className = "profile-headline-title")
    private WebElement consultantDetailsTitle;

    @FindBy(className = "profile-headline-price")
    private WebElement consultantDetailsRate;

    @FindBy(className = "profile-popup-presentation")
    private WebElement shortPresentation;

    @FindBy(className = "profile-popup-suggest-a-mission")
    private WebElement consultantSendEmail;


    // Popup info user need to be connected
    @FindBy(xpath= "//*[@class='app-popup ']")
    private WebElement popupInfoConnexion;

    @FindBy(xpath= "//*[@class='app-popup ']//img[@class='app-popup-header-close']")
    private WebElement closePopupInfoConnexion;


    @FindBy(xpath= "//*[@class='app-popup ']//a")
    private WebElement linkToConnect;

    @FindBy(xpath= "//*[@class='app-popup ']//div[contains(@class,'button')]")
    private WebElement buttonOK;


    private String url = EnvironmentVariables.getVariable("url");// Get the url from the right environment

    public BrowsePage(WebDriver driver) {
        super(driver);
    }

    public void loadBrowsePage(){
        driver.get(url+"browse");
        explicityWait().until(ExpectedConditions.visibilityOf(resultsBlock));
    }

    public void waitBrowsePage(){
        explicityWait().until(ExpectedConditions.visibilityOf(resultsBlock));
    }

    public void enterAskill(String skill){
        inputSearch.clear();
        inputSearch.sendKeys(skill);
    }

    public void enterALocalisation(String localisation){
        inputLocation.clear();
        inputLocation.sendKeys(localisation);
    }

    public void selectOfficeWork(){
        selectCheckox(checkboxOfficeWork);
    }

    public void unselectOfficeWork(){
        unselectCheckox(checkboxOfficeWork);
    }

    public void selectRemoteWork(){
        selectCheckox(checkboxOfficeWork);
    }

    public void unselectRemoteWork(){
        unselectCheckox(checkboxOfficeWork);
    }

    public void moveLowerDailyRate(){
    }

    public void moveUpperDailyRate(){
    }

    public int getLowerSliderValue(){
        return (int)Double.parseDouble(lowerDailyRateValue.getText());
    }

    public int getUpperSliderValue(){
        return (int)Double.parseDouble(upperDailyRateValue.getText());
    }

    public int getSliderMinValue(){
        return (int)Double.parseDouble(lowerDailyRateSlider.getAttribute("aria-valuemin"));
    }

    public int getSliderMaxValue(){
        return (int)Double.parseDouble(upperDailyRateSlider.getAttribute("aria-valuemax"));
    }

    public void selectKeyword(String keyword){
        for (WebElement key: keywords){
            if(key.getText().equals(keyword)){
                selectCheckox(key);
            }
        }
    }

    public void unselectKeyword(String keyword){
        for (WebElement key: keywords){
            if(key.getText().equals(keyword)){
                unselectCheckox(key);
            }
        }
    }

    public int getResultCount(){
        String resultCountAllSentence = resultsCount.getText();
        String[] splittedSentence = resultCountAllSentence.split(" ");
        return Integer.valueOf(splittedSentence[0]);
    }

    public int countConsultantBlocks(){
        return consultantBloks.size();
    }

    public ArrayList<String> getConsultantsName(){
        ArrayList<String> names = new ArrayList<String>();
        for(WebElement name: consultantsName){
           names.add(name.getText());
        }
        return names;
    }

    public ArrayList<String> getConsultantsTitle(){
        ArrayList<String> titles = new ArrayList<String>();
        for(WebElement title: consultantsTitle){
            titles.add(title.getText());
        }
        return titles;
    }

    public ArrayList<String> getConsultantsDailyRate(){
        ArrayList<String> dailyRates = new ArrayList<String>();
        for(WebElement dailyRate: consultantsDailyRate){
            dailyRates.add(dailyRate.getText());
        }
        return dailyRates;
    }

    public ArrayList<String> getConsultantsExperience(){
        ArrayList<String> experiences = new ArrayList<String>();
        for(WebElement experience: consultantsExperience){
            experiences.add(experience.getText());
        }
        return experiences;
    }

    public void verifyKeywordForEachConsultant(String keyword){
        // For each consultant we check if he has the keyword.
        for(WebElement consultant: consultantBloks){
            int count = 0;
            //We search for the keywords on each consultant
            List<WebElement> keywords = consultant.findElements(By.className("tag"));
            for (WebElement key: keywords){
                // If the keyword is present, we add 1 to the count
                if (key.getText().equals(keyword)){
                    count++;
                }
            }
            // The count must be equal to 1 for each consutant
            Assert.assertTrue(count==1);
        }
    }

    public void clickOnRandomConsultant(){
        Random rd = new Random();
        int indexRd = rd.nextInt(consultantBloks.size());
        consultantBloks.get(indexRd).click();
        sleep(5);
    }

    public void displayConsultantDetails(){
        clickOnRandomConsultant();
        explicityWait(2).until(ExpectedConditions.visibilityOf(popupConsultantDetails));
    }

    public boolean isConsultantDetailsDisplayed(){
        return popupConsultantDetails.isDisplayed();
    }

    public void closeConsultantDetails(){
        closeConsultantDetails.click();
    }

    public String getNameFromDetails(){
       return consultantDetailsName.getText();
    }

    public String getTitleFromDetails(){
        return consultantDetailsTitle.getText();
    }

    public String getDailyRateFromDetails(){
        return consultantDetailsRate.getText();
    }

    public String getPresentationFromDetails(){
        return shortPresentation.getText();
    }

    public void clickOnSendEmail(){
        consultantSendEmail.click();
    }

    public void displayPopinInfoConnexion(){
        clickOnSendEmail();
        explicityWait(2).until(ExpectedConditions.visibilityOf(popupInfoConnexion));
    }

    public boolean isInfoConnexionDisplayed(){
        return popupInfoConnexion.isDisplayed();
    }

    public void closeInfoConnexionByCross(){
        closePopupInfoConnexion.click();
    }

    public void closeInfoConnexionByOk(){
        buttonOK.click();
    }

    public void clickOnConnectLink(){
        linkToConnect.click();
    }

}

