package Pages;

import AbstractClass.GenericsSelenuimMethods;
import Configs.EnvironmentVariables;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginPage extends GenericsSelenuimMethods {

    @FindBy(className = "login-page-form")
    private WebElement loginForm;

    @FindBy(className = "email")
    private WebElement emailField;

    @FindBy(className = "password")
    private WebElement passwordField;

    @FindBy(className = "validate-button")
    private WebElement validateButton;

    @FindBy(className = "password-forgot")
    private WebElement resetLink;

    //Popin reset password
    @FindBy(className = "emailrecup")
    private WebElement inputResetEmail;

    @FindBy(css ="button[type='submit']")
    private WebElement submitResetButton;

    // Error Popin
    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]")
    private WebElement errorPopin;

    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]//img")
    private WebElement closeButtonOfErrorPopin;

    @FindBy(xpath ="(//div[contains(@class, 'app-popup--show')][1]//div[@style])[1]")
    private WebElement contentOfErrorPopin;

    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]//div[contains(@class, 'button')]")
    private WebElement okButtonOfErrorPopin;
    
    private String url = EnvironmentVariables.getVariable("url");// Get the url from the right environment

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void load(){
        driver.get(url+"login");
        explicityWait().until(ExpectedConditions.visibilityOf(loginForm));

    }

    public void waitPage(){
        explicityWait().until(ExpectedConditions.visibilityOf(loginForm));
    }

    public void enterEmail(String email){
        emailField.clear();
        emailField.sendKeys(email);
    }

    public void enterPassword(String password){
        passwordField.clear();
        passwordField.sendKeys(password);
    }

    public void submitSignIn(){
        validateButton.click();
    }

    public void clickOnResetLink(){
        resetLink.click();
        explicityWait().until(ExpectedConditions.visibilityOf(inputResetEmail));
    }

    public boolean isResetPopinDisplayed(){
        return isElementDisplayed(inputResetEmail);
    }

    public void enterEmailToReset(String emailToReset){
        inputResetEmail.clear();
        inputResetEmail.sendKeys(emailToReset);
    }

    public void submitReset(){
        submitResetButton.click();
    }

    public void signin(String email, String password){
        load();
        enterEmail(email);
        enterPassword(password);
        submitSignIn();
        // After each connection, the user is redirect to the profile page.
        explicityWait().until(ExpectedConditions.urlContains("my-profile"));
    }

    public String getErrorMessage(){
        explicityWait().until(ExpectedConditions.visibilityOf(errorPopin));
        return contentOfErrorPopin.getText();
    }

    public void validErrorPopin(){
        okButtonOfErrorPopin.click();
        explicityWait(1).until(ExpectedConditions.invisibilityOf(errorPopin));
    }

}
