package Pages;

import AbstractClass.GenericsSelenuimMethods;
import Configs.EnvironmentVariables;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;

public class MissionPage extends GenericsSelenuimMethods {
    @FindBy (className = "missions-page")
    private WebElement missionPage;

    @FindBy (className = "no-missions-message")
    private WebElement noMission;

    @FindBy (className = "mission" )
    private List<WebElement> missions;

    @FindBy (className = "mission-status")
    private List<WebElement> missionsStatuses;

    @FindBy (className = "mission-peer-picture")
    private List<WebElement> peerPicture;

    @FindBy (className = "mission-peer-name")
    private List<WebElement> peerName;

    @FindBy (className = "mission-peer-see-profile")
    private List<WebElement> seeProfileLink;

    @FindBy (className = "mission-title")
    private List<WebElement> missionTitle;

    @FindBy (className = "mission-days")
    private List<WebElement> missionDuration;

    @FindBy (className = "mission-price")
    private List<WebElement> missionDailyPrice;

    @FindBy (css = ".mission-info div:nth-child(4)>div")
    private List<WebElement> missionTotalPrice;

    @FindBy (className = "start-mission-button")
    private List<WebElement> startMissionButton;

    @FindBy (css = ".mission *[href*='conversations']")
    private List<WebElement> sendMessageButton;

    @FindBy (className = "mission-details-button")
    private List<WebElement> detailsButton;

    // popin mission details
    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]")
    private WebElement popinDetails;

    @FindBy(xpath ="//div[contains(@class, 'app-popup--show')][1]//img")
    private WebElement detailsCloseButton;

    @FindBy(className = "peer-profile-picture")
    private WebElement peerPictureInDetails;

    @FindBy(css = ".mission-popup-left .name")
    private WebElement nameInDetails;

    @FindBy(css = ".peer-mission-details a")
    private WebElement sendMessageFromDetails;

    @FindBy(className = "status")
    private WebElement statusInDetails;

    @FindBy(css = ".mission-popup-right h1")
    private WebElement titleInDetails;

    @FindBy(className = "description")
    private WebElement descriptionInDetails;

    @FindBy(className = "total-price")
    private WebElement totalPriceInDetails;

    @FindBy(className = "price")
    private WebElement priceInDetails;

    @FindBy(className = "event")
    private List<WebElement> eventsHistoric;

    @FindBy(className = "time")
    private List<WebElement> eventsTimes;

    @FindBy(className = "user")
    private List<WebElement> eventsUsers;

    @FindBy(className = "action")
    private List<WebElement> eventsActions;

    @FindBy(className = "startMission")
    private WebElement startMissionFromDetails;

    @FindBy(className = "cancelMission")
    private WebElement cancelMissionFromDetails;

    @FindBy(className = "terminateMission")
    private WebElement terminateMissionFromDetails;

    String url = EnvironmentVariables.getVariable("url");// Get the url from the right environment


    public MissionPage(WebDriver driver) {
        super(driver);
    }

    public void load(){
        driver.get(url+"missions");
        explicityWait().until(ExpectedConditions.visibilityOf(missionPage));
    }

    public void waitMissionPage(){
        explicityWait().until(ExpectedConditions.visibilityOf(missionPage));
    }

    public boolean isMissionPageEmpty(){
        return isElementDisplayed(noMission);
    }

    public int getNumberOfMissions(){
        return missions.size();
    }

    public ArrayList<String> getMissionsStatuses(){
        ArrayList<String> listOfStatuses = new ArrayList<>();
        for(WebElement status: missionsStatuses){
            listOfStatuses.add(status.getText());
        }
        return listOfStatuses;
    }

    public ArrayList<String> getPeersNames(){
        ArrayList<String> listOfNames = new ArrayList<>();
        for(WebElement name : peerName){
            listOfNames.add(name.getText());
        }
        return listOfNames;
    }

    public void seeProfile(int ProfileIndex){
        seeProfileLink.get(ProfileIndex).click();
    }

    public ArrayList<String> getMissionsTitles(){
        ArrayList<String> listOfTitles = new ArrayList<>();
        for(WebElement title : missionsStatuses){
            listOfTitles.add(title.getText());
        }
        return listOfTitles;
    }

    public ArrayList<String> getMissionsDuration(){
        ArrayList<String> listOfDuration = new ArrayList<>();
        for(WebElement duration : missionDuration){
            listOfDuration.add(duration.getText());
        }
        return listOfDuration;
    }

    public ArrayList<String> getMissionsDailyPrice(){
        ArrayList<String> listOfDailyPrice = new ArrayList<>();
        for(WebElement price : missionDailyPrice){
            listOfDailyPrice.add(price.getText());
        }
        return listOfDailyPrice;
    }

    public ArrayList<String> getMissionsToll(){
        ArrayList<String> listOfTolls = new ArrayList<>();
        for(WebElement price : missionTotalPrice){
            listOfTolls.add(price.getText());
        }
        return listOfTolls;
    }

    public void clickOnStartMission(int index){
        startMissionButton.get(index).click();
    }

    public void clickOnSendMessage(int index){
        sendMessageButton.get(index).click();
    }

    public void displayDetailsOfMission(int missionIndex){
        detailsButton.get(missionIndex).click();
        explicityWait().until(ExpectedConditions.visibilityOf(popinDetails));
    }

    public boolean isMissionDetailsDisplayed(){
        return isElementDisplayed(popinDetails);
    }

    public void closeDetails(){
        detailsCloseButton.click();
        explicityWait().until(ExpectedConditions.invisibilityOf(popinDetails));
    }

    public String getNameFromDetails(){
        return nameInDetails.getText();
    }

    public void clickOnSenMessagesFromDetails(){
        sendMessageFromDetails.click();
    }

    public String getStatusFromDetails(){
        return statusInDetails.getText();
    }

    public String getTitleFromDetails(){
        return titleInDetails.getText();
    }

    public String getDescriptionFromDetails(){
        return descriptionInDetails.getText();
    }

    public String getTollFromDetails(){
        return totalPriceInDetails.getText();
    }

    public String getDailyPriceFromDetails(){
        return priceInDetails.getText();
    }

    public int getNumberOfEvents(){
       return  eventsHistoric.size();
    }

    public ArrayList<String> getEventsDate(){
        ArrayList<String> listOfDates = new ArrayList<>();
        for(WebElement date: eventsTimes){
            listOfDates.add(date.getText());
        }
        return listOfDates;
    }

    public ArrayList<String> getEventsOwners(){
        ArrayList<String> listOfOwners = new ArrayList<>();
        for(WebElement owner: eventsUsers){
            listOfOwners.add(owner.getText());
        }
        return listOfOwners;
    }

    public ArrayList<String> getEventsActions(){
        ArrayList<String> listOfActions = new ArrayList<>();
        for(WebElement action: eventsActions){
            listOfActions.add(action.getText());
        }
        return listOfActions;
    }

    public void startTheMissionFromDetails(){
        startMissionFromDetails.click();
    }

    public void cancelTheMission(){
        cancelMissionFromDetails.click();
    }

    public void terminateTheMission(){
        terminateMissionFromDetails.click();
    }
}
