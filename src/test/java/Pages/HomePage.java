package Pages;

import AbstractClass.GenericsSelenuimMethods;

import Configs.EnvironmentVariables;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class HomePage extends GenericsSelenuimMethods {

    @FindBy(css = ".home-logo-large")
    private WebElement titleHP;

    @FindBy(css = "*[onclick*='login']")
    private WebElement headerLoginButton;

    @FindBy(css = "*[onclick*='register']")
    private WebElement headerRegisterButton;

    @FindBy(className= "home-top-logo")
    private WebElement logo;

    @FindBy(className= "search")
    private WebElement searchField;

    @FindBy(className= "location")
    private WebElement geoLocationField;

    @FindBy(css= ".search-container .button")
    private WebElement searchButton;

    @FindBy(xpath= "//*[@class='section4'][1]//a")
    private WebElement bodyFreelanceRegisterButton;

    @FindBy(xpath= "(//*[contains(@class,'section4')]//a)[2]")
    private WebElement bodyRecruiterRegisterButton;

    @FindBy(css = "a[href*='cgu']")
    private WebElement cguLink;

    @FindBy(className = ".app-popup")
    private WebElement popupEmptySearch;

    @FindBy(css = ".app-popup .button")
    private WebElement buttonPopupEmptySearch;


    private String url = EnvironmentVariables.getVariable("url");// Get the url from the right environment


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void load() {
        driver.get(this.url);
        explicityWait().until(ExpectedConditions.visibilityOf(titleHP));
    }

    public void waitHomePage(){
        explicityWait().until(ExpectedConditions.visibilityOf(titleHP));
    }


    public void enterJobTitle(String jobTitle){
        searchField.clear();
        searchField.sendKeys(jobTitle);
    }

    public void enterLocalisation(String localisation){
        geoLocationField.clear();
        geoLocationField.sendKeys(localisation);
    }

    public void clickOnSearchButton(){
        searchButton.click();
    }
    public void searchFromHP(String jobTitle, String localisation){
        enterJobTitle(jobTitle);
        enterLocalisation(localisation);
        clickOnSearchButton();
    }

    public void verifyPopupEmptySearchDisplayed(){
        explicityWait().until(ExpectedConditions.visibilityOf(popupEmptySearch));
        Assert.assertTrue(popupEmptySearch.isDisplayed());
    }

    public void verifyPopupEmptySearchNotDisplayed(){
        explicityWait().until(ExpectedConditions.visibilityOf(popupEmptySearch));
        Assert.assertFalse(popupEmptySearch.isDisplayed());
    }

    public void closePopupEmptySearch(){
        explicityWait().until(ExpectedConditions.visibilityOf(popupEmptySearch));
        buttonPopupEmptySearch.click();
    }

    public void clickOnFreelanceRegisterButton(){
        bodyFreelanceRegisterButton.click();
    }

    public void clickOnRecruiterRegisterButton(){
        bodyRecruiterRegisterButton.click();
    }

    public void clickOnCGULink(){
        cguLink.click();
    }

}
